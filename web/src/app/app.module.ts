import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './users/login/login.component';
import { HeaderComponent } from './sharedModule/header/header.component';
import { CollapseModule } from 'ngx-bootstrap';
import { RegisterComponent } from './users/register/register.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { UserService } from './users/services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from "ngx-toastr";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './users/services/auth-interceptor';
import { AuthGuard } from './users/services/auth.guard';
import { ProfileComponent } from './users/profile/profile.component';
import { PostComponent } from './post/post.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AddpostComponent } from './post/addpost/addpost.component'

const routes : Routes = [
  {
    path : 'home',
    component : HomeComponent,
    canActivate : [AuthGuard]
  },
  {
    path : 'about',
    component : AboutComponent,
    canActivate : [AuthGuard]
  },
  {
    path : '',
    component : LoginComponent
  },
  {
    path : 'register',
    component : RegisterComponent
  },
  {
    path: "profile",
    component: ProfileComponent,
    canActivate : [AuthGuard]
  },
  {
    path: "posts",
    component: PostComponent,
    canActivate : [AuthGuard]
  },
  {
    path : "addPost",
    component : AddpostComponent,
    canActivate : [AuthGuard]
  },
  { 
    path: '**', 
    redirectTo : '/home'
  },
  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    HomeComponent,
    AboutComponent,
    ProfileComponent,
    PostComponent,
    AddpostComponent,
  ],
  imports: [
    BrowserModule,
    CollapseModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [
    UserService,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi : true
    },
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
