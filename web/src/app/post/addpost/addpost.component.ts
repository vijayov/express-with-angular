import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PostService } from 'src/app/post/post.service';
import { UserService } from 'src/app/users/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {
  private createPostForm : FormGroup;
  private isSubmitted : Boolean = false;
  private imagePreview : any;
  
  constructor(
    private formBuilder : FormBuilder,
    private postService : PostService,
    private userService : UserService,
    private router : Router) { }

  ngOnInit() {
    this.createPostForm = this.formBuilder.group({
      title: ['' , Validators.required],
      content: ['' , Validators.required],
      image : ['' , Validators.required]
    });
  }

  onImagePicked(event){
    console.log(event);
    const file = event.target.files[0];
    this.createPostForm.patchValue({image : file});
    this.createPostForm.get('image').updateValueAndValidity;
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
    }
    reader.readAsDataURL(file);
  }

  createPost(){
    console.log(this.createPostForm.value);
    let params = {
      title : this.createPostForm.value.title,
      content : this.createPostForm.value.content,
      image : this.createPostForm.value.image,
      userId : this.userService.getUserId()
    }
    console.log(params);
    this.isSubmitted = true;
    if(this.createPostForm.valid){
      this.postService.addPost(params)
      .then(response => {
        console.log(response);
        this.userService.toastr.success('post added successfully');
        this.router.navigate(['posts'])
      })
      .catch(err => {
        console.log(err);
        this.userService.toastr.error('try after sometime')
      })
    }
  }

}
