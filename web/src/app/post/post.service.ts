import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiUrl = environment.apiURL;

  constructor(private http : HttpClient) { }

  addPost(params){
    console.log(params);
    const postData = new FormData();
    postData.append('title' , params.title);
    postData.append('content' , params.content);
    postData.append('image' , params.image , params.title);
    postData.append('userId' , params.userId);
    console.log(postData.getAll('image'));
    return  this.http.post(`${this.apiUrl}post/createPost` , postData)
    .toPromise();
  }

  getPosts(params){
    let options = {
      params : params,
      headers : new HttpHeaders({
        'Content-type' : 'application/json'
      })
    }
    return this.http.get<{data : Array<Object>}>(this.apiUrl + 'post/getPosts' , options)
    .toPromise();
  }

}
