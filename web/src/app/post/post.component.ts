import { Component, OnInit } from '@angular/core';
import { PostService } from './post.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/users/services/user.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  private posts : Array<Object>;

 
  constructor(private postService : PostService,private userService : UserService) { }

  ngOnInit() {
    this.getPosts();
  }

  createPost(){
    let params = {}
    this.postService.addPost(params)
    .then(response =>{
      console.log(response);
    })
  }
  getPosts(){
    let apiUrl = this.userService.apiURL;
    let params = {
      userId : this.userService.getUserId()
    }
    this.postService.getPosts(params)
    .then(response => {
      console.log(response);
      this.posts = response.data;
    })
  }

}
