import { Component, OnInit , OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from '../../users/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isCollapsed = false;
  private isUserAuthenticated = false;
  private authListnerSub : Subscription;

  constructor(private userService : UserService) { }

  ngOnInit() {
    this.isUserAuthenticated = this.userService.isAuth();
    this.authListnerSub = this.userService.getAuthStatusLister()
    .subscribe(userAuthenticated => {
      this.isUserAuthenticated = userAuthenticated;
    });
  }

  ngOnDestroy(){
    this.authListnerSub.unsubscribe();
  }
  
  logout(){
    this.userService.logout();
  }

}
