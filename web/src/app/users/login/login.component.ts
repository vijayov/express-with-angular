import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user : object = {};

  constructor(private userService : UserService,private router : Router) { }

  ngOnInit() {
    const isAuth = this.userService.isAuth();
    if(isAuth){
      this.router.navigate(['home'])
    }
  }
  loginUser(formDetails : NgForm){
    let params = {
      email : formDetails.value.email,
      password : formDetails.value.password
    }
      this.userService.loginUser(params)
      
  }
}
