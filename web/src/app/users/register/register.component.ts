import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { error } from '@angular/compiler/src/util';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public userService : UserService, public router : Router,private toastr : ToastrService) { }

  ngOnInit() {
    const isAuth = this.userService.isAuth();
    if (isAuth) {
      this.router.navigate(['home'])
    }
  }
  user : object = {};  
  passwordMismatch : boolean = false;

  registerUser(formDetails : NgForm){
    if(formDetails.value.password !== formDetails.value.password1){
      this.passwordMismatch = true;
    }else{
      this.passwordMismatch = false;
    }
    if(formDetails.valid && !this.passwordMismatch){
      let params = {
        name : formDetails.value.name,
        email: formDetails.value.email,
        password: formDetails.value.password  
      }          
        this.userService.registerUser(params)
          .subscribe(
            response => {
              this.toastr.success('User Registered Successfully', '', {
                timeOut: 4000
              })
              this.router.navigate(['/']);
              console.log(response);
            },
            (error) => {
              this.toastr.error (error.json().error_msg, '', {
                timeOut: 4000
              })
              console.log(error.json()); 
            })
    }
  }

}
