import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiURL = environment.apiURL;
  private isAuthenticated : boolean;
  private authStatusListener = new Subject<boolean>();
  helperService = new JwtHelperService();

  constructor(
    private http: HttpClient,
    private router: Router,
    public toastr: ToastrService) { }

  getToken(){
    return localStorage.getItem('token');
  }
  getUserId(){
    return localStorage.getItem('userId')
  }

  isAuth(){
    let token = this.getToken();
    let userId = this.getUserId();
    const decodedToken = this.helperService.decodeToken(token);
    if(decodedToken && decodedToken.id === userId){
      return true
    }
    return false;
  }



  registerUser(params){
    console.log(params);
    return this.http.post(this.apiURL + "user/registerUser" , params);
  }

  getAuthStatusLister(){
    return this.authStatusListener.asObservable();
  }

  loginUser(params){
    this.http.post<{token : string , user : any}>(this.apiURL + 'user/login' , params)
    .subscribe(
      response => {
        console.log(response);
        console.log(response.token);
        this.isAuthenticated = true;
        this.setAuthData(response.token,response.user.id);
        this.authStatusListener.next(true);
        this.router.navigate(['/home']);
      },
      error => {
        console.log(error.error.error_msg)
        this.toastr.error(error.error.error_msg)
      }
    );
  }


  setAuthData(token , id){
    console.log(id);
    localStorage.setItem('token' , token);
    localStorage.setItem('userId' , id);
  }

  removeAuthData(){
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
  }

  getProfileData(userId){
    console.log(userId);
    let options = {
      params : {
        userId : userId
      },
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
    return this.http.get<{ data : {
      name : string,
      email : string,
      _id : string
    } }>(this.apiURL + 'user/profile' , options).toPromise();
  }

  updateProfile(params){
    let options = {
      params : params,
      headers : new HttpHeaders({'Content-type' : 'application/json'})
    }
    return this.http.put(`${this.apiURL}user/updateUserDetails` , options)
    .toPromise();
  }

  logout(){
    this.removeAuthData();
    this.authStatusListener.next(false);
    this.isAuthenticated = false;
    this.router.navigate(['/']);
  }
}
