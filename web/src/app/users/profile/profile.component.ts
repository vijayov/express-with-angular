import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/users/services/user.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private currentUser = localStorage.getItem('userId');
  private userDetails : any = {};

  constructor(private userService : UserService) { }

  ngOnInit() {
    this.getProfileData(this.currentUser);
  }

  getProfileData(currentUser){
    this.userService.getProfileData(currentUser)
    .then(user => {
      console.log(user)
      this.userDetails = user.data;
    })
  }

  updateUser(formDetails : NgForm){
    let params = {
      name : formDetails.value.name,
      email : formDetails.value.email,
      phone : formDetails.value.phone,
      address : formDetails.value.address,
      _id : this.userDetails._id
    }
    if(formDetails.valid){
      this.userService.updateProfile(params)
      .then(response =>{
        console.log(response);
        this.userService.toastr.success('User Details Updated SuccessFully')
      })
    }
  }
}
