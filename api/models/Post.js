const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title : {
        type : String,
        required : true
    },
    content : {
        type : String,
        required : true
    },
    image : {
        type : String,
        required : true
    },
    userId : {
        type : String,
        required : true
    },
    createdAt : {
        type : Date,
        default : Date.now()
    }
})

mongoose.model('posts' , PostSchema);