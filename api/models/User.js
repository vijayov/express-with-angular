const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    phone : {
        type : String
    },
    address : {
        type : String
    },   
    createdAt : {
        type : Date,
        default : Date.now
    }
});
//defining model
mongoose.model('users' , UserSchema);



