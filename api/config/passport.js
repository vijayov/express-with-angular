const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const User = mongoose.model('users');

module.exports = function(passport){
    passport.use(new LocalStrategy(
        {
            usernameField : 'email'
        },
       function(email,password,done){
           User.findOne({email : email})
           .then(user => {
               if(!user){
                   return done(null , false)
               }else{
                  bcrypt.compare(password,user.password, function(req,res){
                      if(res){
                          return done(null , user);
                      }else{
                          return done(null , false)
                      }
                  })
               }
           })
           .catch(err =>{
               return done(err)
           })
       }
    ))
}