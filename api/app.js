const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const app = express();

app.use(cors());

//Mongodb connection
mongoose.connect("mongodb://localhost/expressWithAngular")
.then(() => {
    console.log("mongodb connected");
})

//bodyparser middleware 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//static file middleware
app.use('/assets', express.static(__dirname + '/assets'));

//load routes
const users = require("./routes/users");
const posts = require('./routes/posts');

//include passport
require("./config/passport")(passport);


// use routes
app.use('/user' , users);
app.use('/post' , posts);

app.get('' ,(req,res) => {
    res.send('home');
})

const port = 5000;
app.listen(port   , () => {
    console.log(`server is running in port ${port}`);
})