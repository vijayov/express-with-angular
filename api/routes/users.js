const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const authGuard = require('../middleware/auth-check');


//user model init
require('./../models/User');
const User = mongoose.model('users');


router.post('/registerUser', (req , res) => {
    let allData = req.body;
    const newUser = new User({
        name : allData.name,
        email : allData.email,
        password : allData.password
    })
    bcrypt.genSalt(10,function(err ,salt){
        bcrypt.hash(allData.password,salt,function(err,hash){
            if(err){
                throw err;
            }else{
                newUser.password = hash;
                User.findOne({email : allData.email})
                .then(user => {
                    if(user){
                        console.log(`user ${user}` )
                        res.status(400)
                        .json({
                            error_msg : "User already exists"
                        })
                    }else{
                        newUser.save()
                            .then(user => {
                                res.status(200).json({
                                    data: user
                                })
                            })
                            .catch(err => {
                                res.status(400).json({
                                    error: err
                                })
                            })
                    }
                })
                
            }
        })
    })
})

router.post('/login' , (req,res,next)=>{
    passport.authenticate('local',function(err,user,info){
        console.log(user);
        if(user){
          let token =  jwt.sign({email : user.email,id : user._id} , 'secret');
          let userDetails = {};
          userDetails.name = user.name;
          userDetails.id = user._id;
          res.status(200)
          .json({
              token : token,
              user: userDetails
          })
        }else{
            res.status(400)
            .json({
                error_msg : 'Invalid Username or password'
            })
        }
    })(req, res, next);
})

router.get('/profile' ,authGuard, (req,res) => {
  let allData = req.query;
  User.findOne({_id : allData.userId} ,['name' ,'email','address','phone'])
  .then(response => {
      res.status(200)
      .json({
          data : response
      })
  })
  .catch(err => {
      res.status(400)
      .json({
          error_msg : err
      })
  })
})

router.put('/updateUserDetails' ,authGuard, (req,res)=>{
    let allData = req.body.params;
    console.log(allData)
    User.update({ _id: allData._id }, { $set: allData })
    .then(response => {
        res.status(200)
        .json({
            data : response
        })
    })
    .catch(err => {
        res.status(400)
        .json({
            error_msg : err
        })
    })
})


module.exports = router;