const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');

const router = express.Router();

require('../models/Post');
const Post = mongoose.model('posts');

const MIME_TYPE_MAP = {
    'image/png' : 'png',
    'image/jpeg' : 'jpg',
    'image/jpg' : 'jpg'
}

        const storage = multer.diskStorage({
            destination : (req, file , cb) => {
                console.log(file);
                cb(null , 'assets/images');
            },
            filename : (req , file , cb) => {
                const name = file.originalname.toLowerCase().split(' ').join('-');
                const ext = MIME_TYPE_MAP[file.mimetype];
                cb(null , name + '-' + Date.now() + '.' + ext);
            }
        })

    router.post('/createPost' , multer({storage : storage}).single('image'), (req,res) => {
        let allData = req.body;
        const image = req.protocol + '://' + req.get('host') + '/assets/images/' + req.file.filename;
        const newPost = new Post({
            title : allData.title,
            content : allData.content,
            userId : allData.userId,
            image: image
        })
        newPost.save()
        .then(response =>{
            res.status(200)
            .json({
                data : response
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400)
            .json({
                error_msg : err
            })
        })
    })

    router.get('/getPosts' , (req,res) => {
        let allData = req.query;
        Post.find({userId : allData.userId})
        .then(response => {
            console.log(response);
            res.status(200)
            .json({
                data : response
            })
        })
        .catch(err => {
            res.status(400)
            .json({
                error_msg : error
            })
        })
    })


module.exports = router;